package textEditor;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

/**
 * Simple GUI for a text editor.
 *
 */
public class HtmlEdit extends JFrame implements DocumentListener {
	private JTextArea textArea;
	private Stack<String> undoStack;
	private Stack<String> redoStack;
	private Stack<String> startTagStack;
	private Stack<String> endTagStack;
	private JButton undoButton;
	private JButton redoButton;
	private JButton undoXButton;
	private JMenuBar menuBar;
	private int maxUndo;
	private boolean undo;
	
	private static final long serialVersionUID = 5514566716849599754L;
	/**
	 * Constructs a new GUI: A TextArea on a ScrollPane and undo buttons.
	 */
	public HtmlEdit() {
		super();
		setTitle("HtmlEdit: simple html editor");
		setSize(800, 600);
		textArea = new JTextArea(30, 80);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		undo = false;
		maxUndo = 10;
		
		//Registration of the callback
		textArea.getDocument().addDocumentListener(this);
		
		undoStack = new StackLL<String>();
		redoStack = new StackLL<String>();
		
		undoButton = new JButton();
		undoButton.addActionListener(new UndoButtonHandler());
		undoButton.setText("Undo");
		undoButton.setBackground(Color.cyan);
		undoButton.setSize(60, 50);
		
		redoButton = new JButton();
		redoButton.addActionListener(new RedoButtonHandler());
		redoButton.setText("Redo");
		redoButton.setBackground(Color.cyan);
		redoButton.setSize(60, 50);
		
		undoXButton = new JButton();
		undoXButton.addActionListener(new UndoXButtonHandler());
		undoXButton.setText("Undo 'x' times");
		undoXButton.setBackground(Color.green);
		undoXButton.setSize(60, 50);
		
		menuBar = new JMenuBar();
		menuBar.add(undoButton);
		menuBar.add(redoButton);
		menuBar.add(undoXButton);
		setJMenuBar(menuBar);
		
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setBounds(0, 20, 800, 580);
		getContentPane().add(scrollPane);
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);		
	}

	/**
	 * Callback when changing an element
	 */
	public void changedUpdate(DocumentEvent ev) {
		pushUndoStack();
	}

	/**
	 * Callback when deleting an element
	 */
	public void removeUpdate(DocumentEvent ev) {
		pushUndoStack();
	}
	
	/**
	 * Callback when inserting an element
	 */
	public void insertUpdate(DocumentEvent ev) {
		pushUndoStack();
		try {
			autoComp(ev);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * inserts ending tags when needed
	 * 
	 * @param ev Documentevent needed to find correct offset
	 * @throws BadLocationException
	 */
	private void autoComp(DocumentEvent ev) throws BadLocationException {
		if(!checkTags()) {
			pushStartTags();
			pushEndTags();
			while(!endTagStack.isEmpty()) {
				if(endTagStack.top().equals(startTagStack.top())) {
					startTagStack.pop();
				}
				endTagStack.pop();
			}
			String s = "</" + startTagStack.top().substring(1);
			SwingUtilities.invokeLater(new Task(s, ev.getOffset() + 1));
		}
	}
	
	/**
	 * 
	 * @param offset is the offset of the beginning of the tag
	 * @return the offset
	 * @throws BadLocationException
	 */
	private int getStartOffset(int offset) throws BadLocationException {
		if(offset < 0) return -1;
		if(textArea.getText(offset, 1).equals("<")) return offset;
		else return getStartOffset(offset - 1);
	}
	
	/**
	 * this method pushes all start tags on a stack
	 * 
	 * @throws BadLocationException
	 */
	private void pushStartTags() throws BadLocationException {
		int offset;
		startTagStack = new StackLL<String>();
		for(offset = 0; offset < textArea.getText().length(); offset++) {
			if(textArea.getText(offset, 1).equals(">")) {
				int tempOffset = getStartOffset(offset);
				if(tempOffset >= 0) {
					if(!textArea.getText(tempOffset + 1, 1).equals("/")) {
						String s = "";
						while(!textArea.getText(tempOffset, 1).equals(" ") && !textArea.getText(tempOffset, 1).equals(">")) {
							s = s + textArea.getText(tempOffset, 1);
							tempOffset++;
						}
						s = s + ">";
						startTagStack.push(s);
					}
				}
			}
		}
	}
	
	/**
	 * this method pushes all end tags on a stack
	 * 
	 * @throws BadLocationException
	 */
	private void pushEndTags() throws BadLocationException {
		int offset;
		endTagStack = new StackLL<String>();
		for(offset = textArea.getText().length(); offset > 0  ; offset--) {
			if(textArea.getText(offset, 1).equals(">")) {
				int tempOffset = getStartOffset(offset) + 1;
				if(textArea.getText(tempOffset, 1).equals("/")) {
					tempOffset++;
					String s = "";
					while(!textArea.getText(tempOffset, 1).equals(" ") && !textArea.getText(tempOffset, 1).equals(">")) {
						s = s + textArea.getText(tempOffset, 1);
						tempOffset++;
					}
					s = "<" + s + ">";
					endTagStack.push(s);
				}
			}
		}
	}
	
	/**
	 * 
	 * @return true if every start tag has an and tag
	 * @throws BadLocationException
	 */
	public boolean checkTags() throws BadLocationException {
		pushStartTags();
		pushEndTags();
		while(!endTagStack.isEmpty()) {
			if(endTagStack.top().equals(startTagStack.top())) {
				startTagStack.pop();
			}
			endTagStack.pop();
		}
		return startTagStack.isEmpty();
		
	}
	
	private void pushUndoStack() {
		if(undo) return;
		undoStack.push(textArea.getText());
		if(undoStack.size() > maxUndo) undoStack.slide();
	}
	
	private class UndoButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String top = undoStack.top();
			if(top == null) return;
			undo = true;
			redoStack.push(top);
			if(redoStack.size() > maxUndo) redoStack.slide();
			undoStack.pop();
			textArea.setText(undoStack.top());
			undo = false;
		}
    }
	
	private class RedoButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String top = redoStack.top();
			if(top == null) return;
			undo = true;
			textArea.setText(top);
			redoStack.pop();
			undo = false;
		}	
    }
	
	private class UndoXButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int x = Integer.parseInt(JOptionPane.showInputDialog(null, "Times to undo"));
			int i = 0;
			while(i < x) {
				undoButton.doClick();
				i++;
			}
		}
    }
	
	private class Task implements Runnable {
		private String text;
		private int offset;
		
		/**
		 * Pass parameters in the Runnable constructor to pass data from the callback 
		 * @param text which will be inserted after the start tag
		 * @param offset where the text has to be inserted
		 */
		Task(String text, int offset) {
			this.text = text;
			this.offset = offset;
		}

		/**
		 * The entry point of the runnable
		 */
		public void run() {
			textArea.insert(text, offset);
			textArea.setCaretPosition(offset);
		}
	}

	/**
	 * Entry point of the application: starts a GUI
	 */
	public static void main(String[] args) {
		new HtmlEdit();
	}

}