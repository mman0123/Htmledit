package textEditor;

import java.util.Iterator;

public class LinkedList<E> implements Iterable<E> {
	private int size;
	private Node head;
	
	public LinkedList() {
		size = 0;
		head = null;
	}
	
	public LinkedList(E element) {
		size = 1;
		head = new Node(element);
	}
	
	private LinkedList(Node node) {
		head = node;
		size = count(node);
	}
	
	private int countR(Node cursor, int count) {
		if(cursor == null) return count;
		return countR(cursor.getNext(), count+1);
	}
	
	public boolean contains(E element) {
		return contains(head, element);
	}
	
	private boolean contains(Node node, E element) {
		if(node == null) return false;
		if(node.getElement() == element) return true;
		return contains(node.getNext(), element);
	}
	
	private int count(Node node) {
		return countR(node, 0);
	}
	
	public int getSize() {
		return size;
	}
			
	public void prepend(E element) {
		Node temp = new Node(element, head);
		head = temp;
		size++;
	}
		
	public E getHead() {
		return head.getElement();
	}
	
	public LinkedList<E> getTail(){
		return new LinkedList<E>(head.getNext());
	}
	
	public E removeLast() {
		E temp = getLast(head).element;
		remove(temp);
		return temp;
	}
	
	private Node getLast(Node cursor) {
		if(cursor == null) return null;
		if(cursor.getNext() == null) return cursor;
		return getLast(cursor.getNext());
	}
	
	public E removeHead() {
		E temp = getHead();
		remove(temp);
		return temp;
	}
	
	public void remove(E element) {
		Node toBeRemoved;
		if(head.getElement().equals(element)) {
			toBeRemoved = head;
			head = toBeRemoved.getNext();
		}
		else {
			Node before = searchNodeBefore(element, head);
			toBeRemoved = before.getNext();
			before.setNext(toBeRemoved.getNext());
		}
		toBeRemoved.setNext(null);
		size--;
	}
	
	private Node searchNodeBefore(E element, Node cursor) {
		if(cursor == null || cursor.getNext() == null) return null;
		if(cursor.getNext().getElement().equals(element)) return cursor;
		return searchNodeBefore(element, cursor.getNext());
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	/**
	 * This is a node containing an element used in a singly linked list
	 * 
	 *
	 * @param <E> type parameter
	 */
	private class Node {
		private E element;
		private Node next;
		
		/***
		 * Creates a node with a single element, next referring to null
		 * 
		 * @param element
		 */
		public Node(E element) {
			this(element, null);
		}
		
		/**
		 * Creates a node with a single element, next referring to the next node
		 * @param element
		 * @param next
		 */
		public Node(E element, Node next) {
			this.element = element;
			this.next = next;
		}
		
		/**
		 * Getter for the element
		 *  
		 * @return element
		 */
		public E getElement() {
			return element;
		}
		
		/**
		 * Getter for the next node
		 * 
		 * @return next node
		 */
		public Node getNext(){
			return next;
		}
		
		/**
		 * Setter for the next node
		 *  
		 * @param next
		 */
		public void setNext(Node next) {
			this.next = next;
		}

	}

	@Override
	public Iterator<E> iterator() {
		return new LinkedListIterator();
	}
	
	private class LinkedListIterator implements  Iterator<E> {
		private Node cursor = head;
		
		/**
		 * Mogelijk alternatief?
		 * 
		@Override
		public boolean hasNext() {
			return !(iterator().next() == null);
		}
		*/
		
		@Override
		public boolean hasNext() {
			 return cursor != null;
		}

		@Override
		public E next() {
			E element = cursor.getElement();
			cursor = cursor.getNext();
			return element;
		}
	
	}
	
}