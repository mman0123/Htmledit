package textEditor;

public class StackLL<E> implements Stack<E> {
	private LinkedList<E> list;
	
	public StackLL(){
		
		list = new LinkedList<E>();
	}
	
	public int size() {
		return list.getSize();
	}
	
	public boolean isEmpty() {
		return list.getSize() == 0;
	}
	
	public void push(E element){
		list.prepend(element);
	}
	
	public E top(){
		if(isEmpty()) return null;
		return list.getHead();
	}
	
	public E pop(){
		if(top() == null) return null;
		return list.removeHead();
	}
	
	public E slide() {
		if(top() == null) return null;
		return list.removeLast();
	}
}
